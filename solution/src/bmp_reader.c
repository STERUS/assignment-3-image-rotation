#include "image_read.h"
#include "malloc.h"
#include <stdio.h>

struct bmp_header {
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
} __attribute__((packed));

static void print_bmp_header(struct bmp_header const* header);
static uint32_t calculate_pixels_count(const struct bmp_header* header);
static uint16_t calculate_padding(uint32_t width);

// Десериализация из формата BMP
enum read_status from_bmp( FILE* in, struct image* img, void** hdr){

    struct bmp_header header;
    *hdr = malloc(sizeof(struct bmp_header));
    size_t l = fread(&header, sizeof(struct bmp_header), 1, in);
    //Инициализируем тут наш глобальный заголовок
    if (hdr == NULL || l != 1){ // Обработка ошибки чтения
        return READ_ERROR;
    }
    fprintf(stdout, "Read Header (%zu element)", l);
    *((struct bmp_header*) *hdr) = header;

    print_bmp_header(&header); // Теперь мы имеем готовый header нашего изображения

    //Считаем количество пикселей и padding
    uint32_t pixels_count = calculate_pixels_count(&header);
    uint16_t padding = calculate_padding(header.biWidth);
    fprintf(stdout, "Number of image pixels: %"PRIu32"\n Padding: %"PRId16"\n", pixels_count, padding);
    //Выделяем всю необходимую память
    img->height = (uint64_t) header.biHeight;
    img->width = (uint64_t) header.biWidth;
    img->data = malloc(sizeof(struct pixel) * pixels_count);
    if(img->data == NULL){
        return READ_ERROR;
    }
    //Устанавливаем курсор в место начала битовой карты
    if(fseek(in, (long) header.bOffBits, SEEK_SET) != 0){
        return READ_ERROR;
    }
    //Проходимся по строчкам заполняя img->data
    size_t counter = 0;
    for(size_t i = 0; i < header.biHeight; i++){
        //Проверяем fread и fseek на ошибки
        if(fread((img->data) + counter, sizeof(struct pixel), header.biWidth, in) != header.biWidth){
            return READ_ERROR;
        }
        if(fseek(in, (long) padding, SEEK_CUR) != 0){
            return READ_ERROR;
        }
        counter = counter + header.biWidth;
    }

    return READ_OK;
}

// Сериализация в формат BMP
enum write_status to_bmp( FILE* out, struct image const* img, void* hdr ){
    //Создаем header для нового файла
    ((struct bmp_header*)hdr)->bOffBits = (uint32_t) sizeof(struct bmp_header);
    ((struct bmp_header*)hdr)->biWidth = (uint32_t) img->width;
    ((struct bmp_header*)hdr)->biHeight = (uint32_t) img->height;
    //print_bmp_header((struct bmp_header*)hdr);
    //Записали header
    if(fwrite(hdr, sizeof(struct bmp_header), 1, out) != 1){
        return WRITE_ERROR;
    }
    uint16_t padding = calculate_padding(img->width);
    size_t counter = 0;
    for(size_t i = 0; i < img->height; i++){
        if(fwrite((img->data) + counter, sizeof(struct pixel), img->width, out) != img->width){
            return WRITE_ERROR;
        }
        if(fseek(out, (long) padding, SEEK_CUR) != 0){
            return WRITE_ERROR;
        }
        counter = counter + img->width;
    }
    return WRITE_OK;

}

static void print_bmp_header(struct bmp_header const* header){
    printf("bfType: %" PRIu16"\n", header->bfType);
    printf("bfileSize: %" PRIu32"\n", header->bfileSize);
    printf("bfReserved: %" PRIu32"\n", header->bfReserved);
    printf("bOffBits: %" PRIu32"\n", header->bOffBits);
    printf("biSize: %" PRIu32"\n", header->biSize);
    printf("biWidth: %" PRIu32"\n", header->biWidth);
    printf("biHeight: %" PRIu32"\n", header->biHeight);
    printf("biPlanes: %" PRIu16"\n", header->biPlanes);
    printf("biBitCount: %" PRIu16"\n", header->biBitCount);
    printf("biCompression: %" PRIu32"\n", header->biCompression);
    printf("biSizeImage: %" PRIu32"\n", header->biSizeImage);
    printf("biXPelsPerMeter: %" PRIu32"\n", header->biXPelsPerMeter);
    printf("biYPelsPerMeter: %" PRIu32"\n", header->biYPelsPerMeter);
    printf("biClrUsed: %" PRIu32"\n", header->biClrUsed);
    printf("biClrImportant: %" PRIu32 "\n", header->biClrImportant);
}

void free_memory(struct image* img){
    if(img != NULL) {
        free(img->data);
    }
}

void free_header(void** header){
    free(*header);
}
static uint32_t calculate_pixels_count(const struct bmp_header* header){
    return header->biWidth * header->biHeight;
}

static uint16_t calculate_padding(uint32_t width){
    if(width % 4 == 0){
        return 0;
    } else {
        return 4 - ((width * 3) % 4);
    }
}
