#include "structures.h"
#include <malloc.h>
//Создадим макрос, чтобы проверять результат malloc на NULL
#define alloc(ptr, size)\
\
    ptr = malloc(size);    \
    if((ptr) == NULL){      \
        return (struct image){0};                    \
    }



static void copy_data(struct image src, struct image new){
    for(size_t i = 0; i < src.width * src.height; i++){
        new.data[i] = src.data[i];
    }
}

static struct image same(const struct image src){
    struct image new_image = { .width = src.width, .height = src.height, .data = NULL};
    alloc( new_image.data, sizeof(struct pixel) * new_image.width * new_image.height);
    copy_data(src, new_image);
    return new_image;
}

static struct image rotate270(const struct image src){
    struct image new_image = { .width = src.height, .height = src.width, .data = NULL};
    alloc( new_image.data, sizeof(struct pixel) * new_image.width * new_image.height);

    size_t counter = 0;
    for(size_t i = 0; i < src.width; i++){
        size_t temp = i + (src.height - 1) * src.width;
        for(size_t j = 0; j < src.height; j++){
            new_image.data[counter] = src.data[temp];
            temp -= src.width;
            counter++;
        }
    }
    return new_image;
}

static struct image rotate90(const struct image src){
    struct image new_image = { .width = src.height, .height = src.width, .data = NULL};
    alloc( new_image.data, sizeof(struct pixel) * new_image.width * new_image.height);

    size_t counter = 0;
    for(size_t i = src.width; i > 0; i--){
        size_t temp = i - 1;
        for(size_t j = 0; j < src.height; j++){
            new_image.data[counter] = src.data[temp];
            temp += src.width;
            counter++;
        }
    }
    return new_image;
}

static struct image rotate180(const struct image src){
    struct image new_image = { .width = src.width, .height = src.height, .data = NULL};
    alloc( new_image.data, sizeof(struct pixel) * new_image.width * new_image.height);

    size_t counter = 0;
    for(size_t i = src.height; i > 0; i--){
        size_t temp = i * src.width - 1;
        for(size_t j = src.width; j > 0; j--){
            new_image.data[counter] = src.data[temp];
            temp -= 1;
            counter++;
        }
    }
    return new_image;
}

struct image rotate(const struct image src, int16_t angle){
    if(angle == 0){
        return same(src);
    }
    if(angle == -90 || angle == 270){
        return rotate270(src);
    }
    if(angle == 90 || angle == -270){
        return rotate90(src);
    }
    if (angle == 180 || angle == -180){
        return rotate180(src);
    }

    return same(src);
}
