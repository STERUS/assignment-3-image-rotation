#ifndef BMPTEST_BMP_READER_H
#include "image_read.h"
#include <stdio.h>
#define BMPTEST_BMP_READER_H
enum read_status from_bmp( FILE* in, struct image* img, void* );
enum write_status to_bmp( FILE* out, struct image const* img, void* );
void free_memory(struct image* img);
void free_header(void**);
#endif //BMPTEST_BMP_READER_H
