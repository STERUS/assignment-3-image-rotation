//
// Created by stepa on 14.11.2023.
//

#ifndef BMPTEST_STRUCTURES_H
#define BMPTEST_STRUCTURES_H
#include <inttypes.h>
struct pixel{
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};
#endif //BMPTEST_STRUCTURES_H
