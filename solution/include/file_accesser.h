#ifndef BMPTEST_FILE_ACCESSER_H
#define BMPTEST_FILE_ACCESSER_H
#include <stdio.h>

enum open_status {
    OPEN_OK = 0,
    OPEN_NULL
};
extern enum open_status open_file_for_read(const char* file_name, FILE** input);
extern enum open_status open_file_for_write(const char* file_name, FILE** output);

enum close_status{
    CLOSE_OK = 0,
    CLOSE_ERROR,
};
extern enum close_status close_file(FILE** input);

#endif //BMPTEST_FILE_ACCESSER_H
