#ifndef BMPTEST_IMAGE_TRANSFORMER_H
#define BMPTEST_IMAGE_TRANSFORMER_H
#ifndef STRUCTURES_H
#include "structures.h"
#endif
struct image rotate(struct image, int16_t);

#endif //BMPTEST_IMAGE_TRANSFORMER_H
